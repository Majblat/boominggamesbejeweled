const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
	entry: './src/BejeweledGame.js',
    devtool: 'cheap-module-eval-source-map',
    plugins: [
        new HtmlWebpackPlugin(),
        new CopyWebpackPlugin([{ from: './/assets', to: './assets' }])
    ],
    devServer: {
        contentBase: './dist'
    },
    module: {
		rules: [
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader']
			},
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: ['babel-loader']
			}
		]
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.js'
	}
};