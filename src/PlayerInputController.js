import DiscretePosition from './DiscretePosition';

class PlayerInputGridController
{
    constructor(stage, grid)
    {
        this._stage = stage;
        this._grid = grid;
        this._mouseReleaseGridPosition = new DiscretePosition();
        this._swapStartPosition = new DiscretePosition(-1, -1);
        this.onPointerDownCallback = this.onPointerDown.bind(this);
        this.onPointerUpCallback = this.onPointerUp.bind(this);
        this.onPointerMoveCallback = this.onPointerMove.bind(this);
        this.isSelectedWithClick = false;
        this.enabled = true;
    }

    onPointerDown(event)
    {
        if (this._grid.inputBlocked)
            return;

        this.mousePressPosition = this._grid.getGridPosition(event.data.getLocalPosition(this._grid.gemContainer));

        if (this.isSelectedWithClick)
        {
            if (this._grid.isSwapValidAtPositions(this.mousePressPosition, this._swapStartPosition))
                this.performSwap(this._swapStartPosition);
            else
                this.selectGem();
        }
        else
        {
            this.selectGem();
        }
    }

    selectGem()
    {
        this._swapStartPosition = this.mousePressPosition.clone();
        this._grid.markGemAtPosition(this.mousePressPosition);
        this.addCallbackOnce("pointerup", this.onPointerUpCallback);
        this.addCallbackOnce("pointermove", this.onPointerMoveCallback);
    }

    onPointerUp(event)
    {
        if (this._grid.inputBlocked)
            return;

        this._mouseReleaseGridPosition = this._grid.getGridPosition(event.data.getLocalPosition(this._grid.gemContainer));

        var mouseReleaseOnTheSameGem = (this.mousePressPosition.x === this._mouseReleaseGridPosition.x && this.mousePressPosition.y === this._mouseReleaseGridPosition.y);

        if (mouseReleaseOnTheSameGem)
        {
            this.isSelectedWithClick = true;
            this._stage.off("pointermove", this.onPointerMoveCallback);
        }
    }

    onPointerMove(event)
    {
        if (this._grid.inputBlocked)
            return;

        var mousePosition = this._grid.getGridPosition(event.data.getLocalPosition(this._grid.gemContainer));

        if (this._grid.isSwapValidAtPositions(this.mousePressPosition, mousePosition))
        {
            this.performSwap(mousePosition);
        }
    }

    performSwap(fromPosition)
    {
        this._grid.swapGemsAtPositions(this.mousePressPosition, fromPosition);
        this.isSelectedWithClick = false;
        this._swapStartPosition = new DiscretePosition(-1, -1);
        this._grid.unmarkSelectedGem();
        this._stage.off("pointerup", this.onPointerUpCallback);
        this._stage.off("pointermove", this.onPointerMoveCallback);
    }

    get enabled()
    {
        return this._enabled;
    }

    set enabled(value)
    {
        this._enabled = value;
        if (this._enabled)
        {
            this.addCallbackOnce("pointerdown", this.onPointerDownCallback);
        }
        else
        {
            this._stage.off("pointerdown", this.onPointerDownCallback);
            this._stage.off("pointerup", this.onPointerUpCallback);
        }
    }

    addCallbackOnce(eventName, callback)
    {
        this._stage.off(eventName, callback);
        this._stage.on(eventName, callback);
    }
}

export default PlayerInputGridController;
