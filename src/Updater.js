class Updater
{

    constructor()
    {
        this._previousTime = 0;
        this._registeredObjects = [];

        window.requestAnimationFrame(this.update.bind(this));
    }

    update(currentTime)
    {
        var deltaTime = (currentTime - this._previousTime) * 0.001;
        this._previousTime = currentTime;


        for (var i = this._registeredObjects.length - 1; i >= 0; i--)
        {
            var updatable = this._registeredObjects[i];
            updatable.update(deltaTime);
        }

        window.requestAnimationFrame(this.update.bind(this));
    }

    add(updatable)
    {
        this._registeredObjects.push(updatable);
    }

    remove(updatable)
    {
        this._registeredObjects.splice(this._registeredObjects.indexOf(updatable), 1);
    }
}

export default Updater;