class GemState
{
    static get STATIONARY() {return 1};

    static get FALLING() {return 2};

    static get DYING() {return 3};

    static get SWAPPING() {return 4};
}


export default GemState;