import PlayerInputGridController from './PlayerInputController';
import * as PIXI from 'pixi.js';
import Grid from './Grid';
import Gem from './Gem';
import TimeUpPopup from './TimeUpPopup';

class BejeweledGame
{
    constructor()
    {
        this.app = new PIXI.Application({width: window.innerWidth, height: window.innerHeight});
        this.app.view.style.position = 'absolute';
        this.app.view.style.left = "0px";
        this.app.view.style.top = '0px';
        this.app.view.style.margin = '0px';
        this.scalingContainer = new PIXI.Container();
        this.highScore = 0;

        PIXI.loader
            .add("assets/BackGround.png")
            .add("assets/SelectedGemMarking.png")
            .add("assets/Blue.png")
            .add("assets/Green.png")
            .add("assets/Purple.png")
            .add("assets/Red.png")
            .add("assets/Yellow.png")
            .load(this.start.bind(this));

        document.body.appendChild(this.app.view);
    }

    start()
    {
        this.app.stage.interactive = true;
        this.createBackground();
        this.createGrid();
        this.createMask();
        this.createScoreLabel();
        this.createTimeLeftLabel();
        this.createTimeUpPopup();

        this.resize();
        this.app.stage.addChild(this.scalingContainer);

        let playerInputController = new PlayerInputGridController(this.app.stage, this.grid);

        window.addEventListener("resize", this.resize.bind(this));
        window.addEventListener('orientationchange', this.resize.bind(this));

        this.score = 0;
        this.timeLeft = 60;

        this.updateTimeCallback = this.updateTime.bind(this);
        this.restartGameCallback = this.restartGame.bind(this);
        this.gemFallingIntroFinishCallback = this.onGemFallingIntroFinish.bind(this);
        this.updateScoreCallback = this.updateScore.bind(this);
        this.grid.on("fallCompleted", this.gemFallingIntroFinishCallback);
    }

    startCountDown()
    {
        this.intervalId = window.setInterval(this.updateTimeCallback, 1000);
    }

    createBackground()
    {
        this.background = new PIXI.Sprite(
            PIXI.loader.resources["assets/BackGround.png"].texture
        );
        this.background.anchor.set(0.5, 0.5);
        this.scalingContainer.addChild(this.background);
    }

    createGrid()
    {
        this.grid = new Grid(8, 8);
        this.grid.x -= this.grid.sizeX * Gem.SIZE * 0.5;
        this.grid.y -= this.grid.sizeY * Gem.SIZE * 0.5;
        this.scalingContainer.addChild(this.grid);
    }

    createMask()
    {
        var mask = new PIXI.Graphics();
        mask.x = 0;
        mask.y = 0;
        mask.beginFill(0);
        mask.drawRect(-this.grid.sizeX * Gem.SIZE * 0.5, -this.grid.sizeY * Gem.SIZE * 0.5, this.grid.sizeX * Gem.SIZE, this.grid.sizeY * Gem.SIZE);
        mask.endFill();
        this.grid.mask = mask;
        this.scalingContainer.addChild(mask);
    }

    resize()
    {
        this.app.renderer.resize(window.innerWidth, window.innerHeight);

        this.scalingContainer.scale.y = window.innerHeight / this.background.height;
        this.scalingContainer.scale.x = this.scalingContainer.scale.y;
        this.scalingContainer.x = window.innerWidth * 0.5;
        this.scalingContainer.y = window.innerHeight * 0.5;

        var realWidth = this.grid.sizeX * Gem.SIZE * this.scalingContainer.scale.x;

        if (realWidth > window.innerWidth)
        {
            this.scalingContainer.scale.x = realWidth / this.background.height * 0.95;
            this.scalingContainer.scale.y = this.scalingContainer.scale.x;
        }

        var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
        if (iOS)
            window.scrollTo(0, 0);
    }

    createScoreLabel()
    {
        this.countsCountLabel = new PIXI.Text();
        this.countsCountLabel.x = this.grid.x + this.grid.sizeX * Gem.SIZE * 0.5;
        this.countsCountLabel.y = this.grid.y - 35;
        this.countsCountLabel.anchor.set(0.5, 0);
        this.scalingContainer.addChild(this.countsCountLabel);
    }

    updateScore(currentStreak)
    {
        var scoreMultiplier = currentStreak - 2;
        this.score += 250 * scoreMultiplier;
    }

    createTimeLeftLabel()
    {
        this.timeLeftLabel = new PIXI.Text();
        this.timeLeftLabel.x = this.grid.x + this.grid.sizeX * Gem.SIZE * 0.5;
        this.timeLeftLabel.y = this.grid.y + this.grid.sizeY * Gem.SIZE + 5;
        this.timeLeftLabel.anchor.set(0.5, 0);
        this.scalingContainer.addChild(this.timeLeftLabel);
    }

    updateTime()
    {
        this.timeLeft--;

        if (this.timeLeft === 0)
        {
            if (this.score > this.highScore)
            {
                this.highScore = this.score;
                this.timeUpPopup.highScoreMode = true
            }
            else
            {
                this.timeUpPopup.highScoreMode = false;
            }
            this.intervalId = window.clearInterval(this.intervalId);
            this.grid.inputBlocked = true;
            this.timeUpPopup.visible = true;
            this.app.stage.on("pointerup", this.restartGameCallback);
        }
    }

    createTimeUpPopup()
    {
        this.timeUpPopup = new TimeUpPopup();
        this.timeUpPopup.visible = false;
        this.scalingContainer.addChild(this.timeUpPopup);
    }

    restartGame(event)
    {
        event.stopPropagation();

        TweenMax.killAll();
        this.grid.numGemsFalling = 0;

        this.app.stage.off("pointerup", this.restartGameCallback);
        this.grid.off("streak", this.updateScoreCallback);
        this.timeLeft = 60;
        this.score = 0;
        this.grid.inputBlocked = false;
        this.timeUpPopup.visible = false;
        this.grid.unmarkSelectedGem();
        this.grid.on("fallCompleted", this.gemFallingIntroFinishCallback);
        this.grid.createRandomLayout();
        this.grid.showStartingGemFall();
    }

    onGemFallingIntroFinish()
    {
        this.grid.on("streak", this.updateScoreCallback);
        this.grid.off("fallCompleted", this.gemFallingIntroFinishCallback);
        this.startCountDown();
    }

    set score(value)
    {
        this._score = value;
        this.countsCountLabel.text = value;
    }

    get score()
    {
        return this._score;
    }

    set timeLeft(value)
    {
        this._timeLeft = value;
        this.timeLeftLabel.text = value;
    }

    get timeLeft()
    {
        return this._timeLeft;
    }
}

let game = new BejeweledGame();
