import * as PIXI from 'pixi.js';
import TweenMax from 'gsap';
import GemFactory from './GemFactory';
import Updater from './Updater';
import GemState from './GemState';
import Gem from './Gem';
import DiscretePosition from './DiscretePosition';

class Grid extends PIXI.Container
{
    constructor(sizeX, sizeY)
    {
        super();
        this._sizeX = sizeX;
        this._sizeY = sizeY;
        this._gemSlots = []
        this._gemFactory = new GemFactory();
        this.gemContainer = new PIXI.Container();
        this.gemContainer.x = Gem.SIZE * 0.5;
        this.gemContainer.y = Gem.SIZE * 0.5;
        this.addChild(this.gemContainer);

        this._selectedGemMarking = new PIXI.Sprite(
            PIXI.loader.resources["assets/SelectedGemMarking.png"].texture
        );
        this._selectedGemMarking.visible = false;
        this._selectedGemMarking.anchor.set(0.5, 0.5);
        this.gemContainer.addChild(this._selectedGemMarking);

        this.numGemsFalling = 0;
        this._updater = new Updater();
        this.createRandomLayout();
        this.showStartingGemFall();
    }

    createRandomLayout()
    {
        while(this.gemContainer.children[0])
        {
            this.gemContainer.removeChild(this.gemContainer.children[0]);
        }

        this._missingGemsPerColumn = [];
        for (var i = 0; i < this._sizeX; i++)
        {
            this._missingGemsPerColumn.push(this._sizeY);
        }

        for (var x = 0; x < this._sizeX; x++)
        {
            this._gemSlots[x] = [];
            for (var y = 0; y < this._sizeY; y++)
            {
                this.addGemAt(this._gemFactory.createGemOfRandomType(), x, y);
            }
        }

        while (this.checkForGemsToRemove(false))
        {
            this.createRandomLayout();
        }
    }

    showStartingGemFall()
    {
        this.inputBlocked = true;

        for (var x = this._sizeX - 1; x >= 0; x--)
        {
            for (var y = this._sizeY - 1; y >= 0; y--)
            {
                var gem = this._gemSlots[x][y];
                gem.currentState = GemState.FALLING;
                gem.fallingStartPosition = -1;
                gem.y = gem.fallingStartPosition * Gem.SIZE;
                gem.fallingPotential = y + 1;

                TweenMax.delayedCall((this._sizeX - x) * 0.1 + (this._sizeY - y) * 0.2, this._updater.add.bind(this._updater), [gem]);
                gem.callback = this.onGemFinishedFalling.bind(this, gem);
                this.numGemsFalling++;
                if (gem.callback)
                    gem.off("fallingFinished", gem.callback);
                gem.on("fallingFinished", gem.callback);
            }
        }

    }

    onGemFinishedFalling(gem)
    {
        this.numGemsFalling--;
        gem.removeAllListeners();
        gem.gemcurrentState = GemState.STATIONARY;
        var x = Math.floor(gem.x / Gem.SIZE);
        var y = Math.floor(gem.y / Gem.SIZE);
        this._gemSlots[x][y] = gem;
        this._updater.remove(gem);
        this.checkForGemsToRemove(true);
    }

    checkForGemsToRemove(runTweens)
    {
        var gemsToRemoveFound = false;

        for (var i = 0; i < this._sizeX; i++)
        {
            if (this.checkRowOrColumn(i, false, runTweens))
                gemsToRemoveFound = true;
        }
        for (i = 0; i < this._sizeY; i++)
        {
            if (this.checkRowOrColumn(i, true, runTweens))
                gemsToRemoveFound = true;
        }

        if (gemsToRemoveFound && runTweens)
        {
            TweenMax.delayedCall(Gem.DYING_TIME, this.startGemFalling.bind(this));
        }
        else
        {
            if (this.numGemsFalling === 0 && runTweens)
            {
                this.inputBlocked = false;
                this.emit("fallCompleted");
            }
        }

        return gemsToRemoveFound;
    }

    checkRowOrColumn(i, row = false, runTweens = true)
    {
        var streakFound = false;

        var currentStreak = 0;
        var maxInnerIndex = row ? this._sizeX : this._sizeY;

        for (var j = 1; j < maxInnerIndex; j++)
        {
            var previousGem = this.getGemAt(i, j - 1, row);
            var currentGem = this.getGemAt(i, j, row);

            if (!currentGem || !previousGem)
            {
                currentStreak = 0;
                continue;
            }

            if (currentGem.type === previousGem.type)
            {
                if (currentStreak < 2)
                {
                    currentStreak = 2;
                }
                else
                {
                    currentStreak++;
                    var nextGem = null;
                    var nextGemInnerIndex = j + 1;

                    if (nextGemInnerIndex < maxInnerIndex)
                    {
                        nextGem = this.getGemAt(i, nextGemInnerIndex, row);
                    }

                    if (currentStreak >= 3 && (!nextGem || nextGem.type != currentGem.type))
                    {
                        streakFound = true;
                        var streakStart = j - currentStreak + 1;
                        for (var k = streakStart; k <= j; k++)
                        {
                            this.removeGemAt(i, k, row, runTweens);
                        }

                        this.emit("streak", currentStreak);

                        currentStreak = 0;
                    }
                }
            }
            else
            {
                currentStreak = 0;
            }
        }
        return streakFound;
    }

    isSwapValidAtPositions(firstPosition, secondPosition)
    {
        if (!this.isValidGridPosition(firstPosition))
            return false;

        if (!this.isValidGridPosition(secondPosition))
            return false;

        var distanceX = Math.abs(secondPosition.x - firstPosition.x);
        var distanceY = Math.abs(secondPosition.y - firstPosition.y);

        return (distanceX === 1 && distanceY === 0) || (distanceX === 0 && distanceY === 1);
    }

    isValidGridPosition(gridPosition)
    {
        return gridPosition.x >= 0 && gridPosition.y >= 0 && gridPosition.x < this._sizeX && gridPosition.y < this._sizeY;
    }

    swapGemsAtPositions(firstPosition, secondPosition, swapBackIfNoGemsRemoved = true)
    {
        var firstGem = this._gemSlots[firstPosition.x][firstPosition.y];
        var secondGem = this._gemSlots[secondPosition.x][secondPosition.y];

        if (!firstGem || !secondGem)
            return;

        firstGem.currentState = GemState.SWAPPING;
        secondGem.currentState = GemState.SWAPPING;

        this._gemSlots[firstPosition.x][firstPosition.y] = secondGem;
        this._gemSlots[secondPosition.x][secondPosition.y] = firstGem;

        var tempX = firstGem.x;
        var tempY = firstGem.y;

        this.inputBlocked = true;

        var parameters = { x: secondGem.x, y: secondGem.y};
        if (!swapBackIfNoGemsRemoved)
            parameters.onComplete = this.enableInput.bind(this)

        TweenMax.to(firstGem, Gem.SWAPPING_TIME, parameters);
        TweenMax.to(secondGem, Gem.SWAPPING_TIME, { x: tempX, y: tempY});


        if (swapBackIfNoGemsRemoved)
            TweenMax.delayedCall(Gem.SWAPPING_TIME, this.updateSlots.bind(this), [firstGem, secondGem, firstPosition, secondPosition]);
    }

    enableInput()
    {
        this.inputBlocked = false;
    }

    markGemAtPosition(position)
    {
        this._selectedGemMarking.x = position.x * Gem.SIZE;
        this._selectedGemMarking.y = position.y * Gem.SIZE;
        this._selectedGemMarking.visible = true;
        this.gemContainer.addChild(this._selectedGemMarking);
    }

    unmarkSelectedGem()
    {
        this._selectedGemMarking.visible = false;
    }

    updateSlots(firstGem, secondGem, firstPosition, secondPosition)
    {
        this._gemSlots[firstPosition.x][firstPosition.y] = secondGem;
        this._gemSlots[secondPosition.x][secondPosition.y] = firstGem;

        var gemsFound = this.checkForGemsToRemove(true);

        if (!gemsFound)
        {
            this.swapGemsAtPositions(firstPosition, secondPosition, false);
        }
    }

    startGemFalling()
    {
        this.inputBlocked = true;

        var currentGem;

        for (var x = 0; x < this._sizeX; x++)
        {
            for (var y = 0; y < this._sizeY; y++)
            {
                currentGem = this._gemSlots[x][y];

                if (!currentGem || !currentGem.currentState === GemState.STATIONARY)
                    continue;

                currentGem.fallingPotential = 0;
                currentGem.fallingStartPosition = y;

                for (var k = y + 1; k < this._sizeY; k++)
                {
                    var gemBelow = this._gemSlots[x][k];
                    if (!gemBelow)
                    {
                        currentGem.fallingPotential++;
                        currentGem.currentState = GemState.FALLING;
                    }
                }

                if (currentGem.currentState === GemState.FALLING)
                {
                    this._gemSlots[x][y] = null;
                    this._updater.add(currentGem);
                    currentGem.on("fallingFinished", this.onGemFinishedFalling.bind(this, currentGem));
                    this.numGemsFalling++;
                }
            }
        }

        this.recreateMissingGems();
    }

    recreateMissingGems()
    {
        for (var x = 0; x < this._sizeX; x++)
        {
            var missingGemsInThisColumn = this._missingGemsPerColumn[x];

            for (var y = 0; y < missingGemsInThisColumn; y++)
            {
                var gem = this._gemFactory.createGemOfRandomType();
                gem.currentState = GemState.FALLING;
                gem.fallingStartPosition = -1;
                gem.fallingPotential = (missingGemsInThisColumn - y);
                this.addGemAt(gem, x, gem.fallingStartPosition, false);
                TweenMax.delayedCall(y * 0.2, this._updater.add.bind(this._updater), [gem]);
                gem.on("fallingFinished", this.onGemFinishedFalling.bind(this, gem));
                this.numGemsFalling++;
            }
        }
    }

    getGemAt(x, y, swapIndexes = false)
    {
        if (swapIndexes)
        {
            return this._gemSlots[y][x];
        }
        else
        {
            return this._gemSlots[x][y];
        }
    }

    addGemAt(gem, x, y, addToSlot = true)
    {
        if (addToSlot)
        {
            this._gemSlots[x][y] = gem;
        }
        gem.x = x * Gem.SIZE;
        gem.y = y * Gem.SIZE;
        this.gemContainer.addChild(gem);
        this._missingGemsPerColumn[x]--;
    }

    removeGemAt(x, y, swapIndexes, runTween)
    {
        var outerIndex;
        var innerIndex;

        if (swapIndexes)
        {
            outerIndex = y;
            innerIndex = x;
        }
        else
        {
            outerIndex = x;
            innerIndex = y;
        }

        var gem = this._gemSlots[outerIndex][innerIndex];

        if (!gem)
            return;

        gem.currentState = GemState.DYING;

        if (runTween)
        {
            TweenMax.to(gem.scale, Gem.DYING_TIME, { x: 0, y: 0, onComplete: this.removeGem.bind(this), onCompleteParams: [gem, outerIndex, innerIndex]});
        }
        else
        {
            this.removeGem(gem, outerIndex, innerIndex);
        }
    }

    removeGem(gem, x, y)
    {
        if (gem.parent)
            gem.parent.removeChild(gem);
        this._gemSlots[x][y] = null;
        this._missingGemsPerColumn[x]++;
    }

    getGridPosition(point)
    {
        return new DiscretePosition(Math.floor((point.x + Gem.SIZE * 0.5) / Gem.SIZE), Math.floor((point.y + Gem.SIZE * 0.5) / Gem.SIZE));
    }

    get sizeX()
    {
        return this._sizeX;
    }

    get sizeY()
    {
        return this._sizeY;
    }
}

export default Grid;
