import * as PIXI from 'pixi.js';
import Gem from "./Gem";

class TimeUpPopup extends PIXI.Container
{
    constructor()
    {
        super();

        this.addBackground();

        this.timeUpLabel = new PIXI.Text("", {
            fill:0x000000,
            fontFamily:"Arial",
            fontSize: 50,
            wordWrap:true,
            wordWrapWidth:400,
            align : "center"
        });
        this.hightScoreMode = false;
        this.timeUpLabel.anchor.set(0.5, 0.5);
        this.addChild(this.timeUpLabel);
    }

    addBackground()
    {
        this.background = new PIXI.Graphics();
        this.background.x = 0;
        this.background.y = 0;
        this.background.beginFill(0xCCCCCC, 0.7);
        var halfWidth = 130;
        var halfHeight = 85;
        this.background.drawRect(-halfWidth, -halfHeight, halfWidth * 2, halfHeight * 2);
        this.background.endFill();
        this.addChild(this.background);
    }

    set highScoreMode(value)
    {
        if (value)
            this.timeUpLabel.text = "New high score!\nClick to play again";
        else
            this.timeUpLabel.text = "Time up\nClick to play again";

        this.background.scale.y = value ? 1.5 : 1;
    }
}

export default TimeUpPopup;