import * as PIXI from 'pixi.js';
import GemState from './GemState';

class Gem extends PIXI.Container
{
    constructor(type, imageName)
    {
        super();
        this._type = type;
        this.FALLING_ACCELERATION = 30;
        this._fallingPotential = 0;
        this._fallingVelocity = 0;
        this._fallingStartPosition = 0;

        let gemGraphic = new PIXI.Sprite(
            PIXI.loader.resources["assets/" + imageName + ".png"].texture
        );

        gemGraphic.x -= Gem.SIZE * 0.5;
        gemGraphic.y -= Gem.SIZE * 0.5;
        this.addChild(gemGraphic);

        this._currentState = GemState.STATIONARY;
    }

    get type()
    {
        return this._type;
    }

    get currentState()
    {
        return this._currentState;
    }

    set currentState(value)
    {
        this._currentState = value;
    }

    get fallingPotential()
    {
        return this._fallingPotential;
    }

    set fallingPotential(value)
    {
        this._fallingPotential = value;
    }

    update(dt)
    {
        if (this._fallingPotential > 0)
        {
            this._fallingVelocity += this.FALLING_ACCELERATION * dt;
            this.y += this._fallingVelocity;
        }

        var maxY = (this._fallingStartPosition + this._fallingPotential) * Gem.SIZE;

        if (this.y >= maxY)
        {
            this.y = maxY;
            this.currentState = GemState.STATIONARY;
            this.fallingPotential = 0;
            this._fallingVelocity = 0;
            this.emit("fallingFinished", this);
        }
    }

    get fallingStartPosition()
    {
        return this._fallingStartPosition;
    }

    set fallingStartPosition(value)
    {
        this._fallingStartPosition = value;
    }

    static get SIZE()
    {
        return 55;
    }

    static get SWAPPING_TIME()
    {
        return 0.5;
    }

    static get DYING_TIME()
    {
        return 0.5;
    }
}

export default Gem;
