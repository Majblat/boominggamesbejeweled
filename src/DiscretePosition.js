class DiscretePosition
{
    constructor(x = 0, y = 0)
    {
        this._x = x;
        this._y = y;
    }

    get x()
    {
        return this._x;
    }

    set x(value)
    {
        this._x = value;
    }

    get y()
    {
        return this._y;
    }

    set y(value)
    {
        this._y = value;
    }

    clone()
    {
        return new DiscretePosition(this._x, this._y);
    }
}

export default DiscretePosition;
