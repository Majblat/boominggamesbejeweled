import Gem from './Gem';

class GemFactory
{
    constructor()
    {
        this._typeToGemData =
        [
            "Blue",
            "Green",
            "Purple",
            "Red",
            "Yellow"
        ];
    }

    get numAvailableTypes()
    {
        return this._typeToGemData.length;
    }

    createGem(gemType)
    {
        return new Gem(gemType, this._typeToGemData[gemType]);
    }

    createGemOfRandomType()
    {
        return this.createGem(this.getRandomInteger(0, this.numAvailableTypes));
    }

    getRandomInteger(min, max)
    {
        return Math.floor(Math.random() * (max - min)) + min;
    }
}

export default GemFactory;
