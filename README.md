### How to build the game ###

To build the game, being in game folder - run on command line:
npm install, npm run start

Wait few moments until it compiles

### How to run the game ###

npm start

You can also use my link:
http://piotrmajblat.com/games/bejeweled/

### How to play the game ###

It�s a regular Bejeweled type game so it should be straightforward to play.